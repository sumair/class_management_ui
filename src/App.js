import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Signup from './components/signup/Signup';
import Signin from './components/signin/Signin';

class App extends Component {
  render() {
    return (
        <div>
            <nav>
                <a href="/register">Register</a> |
                <a href="/signin">Signin</a>
            </nav>
            <Router>
                <Switch>
                    <Route exact path="/register" component={Signup} />
                    <Route exact path="/signin" component={Signin} />
                </Switch>
            </Router>
        </div>
    );
  }
}

export default App;
